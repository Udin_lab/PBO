package tes;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class Table extends JPanel{

    JTable jt;
    public Table(){

        String [] header={"name","age"};
        String [][] data={{"akash","20"},{"pankaj","24"},{"pankaj","24"},{"pankaj","24"},{"pankaj","24"}};

        DefaultTableModel model = new DefaultTableModel(data,header);

        JTable table = new JTable(model);
        table.setBackground(new Color(46,43,63));
        table.getTableHeader().setBackground(new Color(60,31,109));
        table.getTableHeader().setPreferredSize(new Dimension (100,40));
        table.setForeground(Color.lightGray);

        table.setPreferredScrollableViewportSize(new Dimension(450,63));
        table.setFillsViewportHeight(true);

        JScrollPane js=new JScrollPane(table);
        js.setBackground(new Color(34,31,46));
        js.setForeground(new Color(34,31,46));
        js.setVisible(true);
        add(js);

    }

    public static void main(String [] args) {

        JFrame jf=new JFrame();
        Table tab= new Table();
        jf.setTitle("Table");
        jf.setSize(500, 500);
        jf.setVisible(true);
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jf.add(tab);

    }

}