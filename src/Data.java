import java.text.SimpleDateFormat;
import java.util.Date;

public class Data {
   public static void main(String[] args) {
      Date date = new Date();
      SimpleDateFormat DateFor = new SimpleDateFormat("dd/MM/yyyy HH:mm");
      String stringDate= DateFor.format(date);
      System.out.println(stringDate);
   }
}
