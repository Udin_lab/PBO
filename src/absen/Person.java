package absen;

public class Person {
   protected String name;
   protected String username;
   protected String password;
   
   Person(String name, String username, String password){
      this.name = name;
      this.username = username;
      this.password = password;
   }

   public void setUsername(String username) {
      this.username = username;
   }
   public void setPassword(String password) {
      this.password = password;
   }
   public void setName(String name) {
      this.name = name;
   }

   public String getName() {
      return name;
   }
   public String getUsername() {
      return username;
   }
   public String getPassword() {
      return password;
   }

}
