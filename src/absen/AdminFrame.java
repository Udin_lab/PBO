package absen;
import java.awt.*;
import javax.swing.*;
public class AdminFrame extends JFrame{
   AdminFrame(){
      this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      this.setSize(new Dimension(600,400));
      this.setResizable(false);
      this.add(new AdminPanel());
      this.setLocationRelativeTo(null);
      this.setVisible(true);
   }
}