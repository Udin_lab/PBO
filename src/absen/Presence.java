package absen;

import java.util.Date;

public class Presence {
   private Date date = new Date();
   private Boolean present = false;
   
   Presence(Boolean present){
      this.present = present;
   }
   public Date getDate() {
      return date;
   }
   public Boolean getPresent() {
      return present;
   }
}
