package absen;

import java.awt.*;
import java.awt.event.*;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.*;
import javax.swing.border.LineBorder;

public class EmployePanel extends JPanel implements ActionListener{
   Color lightOrange = new Color(200, 84, 23 );
   Color darkOrange = new Color(150, 63, 17 );
   Color darkBg= new Color(34,31,46 );
   Color lightBg = new Color(46,43,63 );
   Color lightPurple = new Color(60,31,109 );
   Color darkPurple = new Color(83,31,162 );
   // Color 

   JTextField username;
   JPasswordField password;
   JButton login;
   boolean logedIn = false;
   EmployePanel(){
      this.setLayout(null);
      username = new JTextField();
      password = new JPasswordField();
      login = new JButton("LOGIN");
      login.addActionListener(this);
      DrawUserInput();
   }
   
   public void DrawUserInput(){
      Font font = Main.MyFont();

      password.setBorder(new LineBorder(lightPurple));
      username.setBorder(new LineBorder(lightPurple));
      login.setBorder(new LineBorder(lightPurple));

      username.setBackground(darkBg);
      password.setBackground(darkBg);
      login.setBackground(darkPurple);

      username.setForeground(Color.LIGHT_GRAY);
      password.setForeground(Color.LIGHT_GRAY);
      login.setForeground(Color.lightGray);

      login.setFocusable(false);
      
      username.setCaretColor(Color.lightGray);
      password.setCaretColor(Color.lightGray);

      //labels
      JLabel head = new JLabel("LOGIN SEBAGAI PEGAWAI");
      JLabel userLabel = new JLabel("Username");
      JLabel passLabel = new JLabel("Password");
      
      head.setForeground(Color.LIGHT_GRAY);
      userLabel.setForeground(Color.lightGray);
      passLabel.setForeground(Color.lightGray);
      
      head.setHorizontalAlignment(JLabel.CENTER);
      
      head.setFont(font.deriveFont(Font.BOLD, 20));
      username.setFont(font.deriveFont(Font.PLAIN, 14));
      password.setFont(font.deriveFont(Font.PLAIN, 14));
      userLabel.setFont(font.deriveFont(Font.PLAIN, 14));
      passLabel.setFont(font.deriveFont(Font.PLAIN, 14));
      login.setFont(font.deriveFont(Font.PLAIN, 16));
      
      username.setBounds(240,145,200,35);
      password.setBounds(240,190,200,35);
      login.setBounds(190,250,220,40);

      head.setBounds( 150 , 70, 300, 55);
      userLabel.setBounds(160,145,80,35);
      passLabel.setBounds(160,190,80,35);

      this.add(head);
      this.add(userLabel);
      this.add(passLabel);
      this.add(username);
      this.add(password);
      this.add(login);
   }

   @Override
   public void paintComponent(Graphics g){
      if (!logedIn){
         g.setColor(darkBg);
         g.fillRect(0, 0, 600, 400);

         g.setColor(lightBg);
         //head
         g.fillRect( 150 , 70, 300, 55);
         //main
         g.fillRect( 150 , 135, 300, 100);
         //submit
         g.fillRect(150, 245, 300, 50);
   
         //line
         g.setColor(lightPurple);
         //head
         g.drawRect( 150 , 70, 300, 55);
         //main
         g.drawRect( 150 , 135, 300, 100);
         //submit
         g.drawRect(150, 245, 300, 50);
      }

      if (logedIn){
         g.setColor(darkBg);
         g.fillRect(0, 0, 600, 400);


      }
   }

   @Override
   public void actionPerformed(ActionEvent e) {

      if (e.getSource() == login){
         if (e.getSource() == login){
            String passInput = new String(password.getPassword());
            String userInput = username.getText();
            int index = 0;
            boolean found = false;
            for (int i = 0; i<Main.employes.size(); i++){
               String userData = Main.employes.get(i).getUsername();
               if (userData.equals(userInput)){
                  index = i;
                  found = true;
                  break;
               }
            }

            if (found){
               String passData = Main.employes.get(index).getPassword();
               if (passInput.equals(passData)){
                  logedIn = true;
                  // curretntAdmin = Main.employes.get(index);
                  removeAll();
                  repaint();
               }
               else{
                  JOptionPane.showMessageDialog(this, "Username dan password admin tidak cocok","", JOptionPane.ERROR_MESSAGE);
               }
            }
            else{
               JOptionPane.showMessageDialog(this, "Admin tidak ditemukan, masukan akun dengan benar", "Admin tidak ditemukan", JOptionPane.ERROR_MESSAGE);
            }
         }
        
      }
   }
}
