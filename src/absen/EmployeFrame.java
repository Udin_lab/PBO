package absen;
import java.awt.*;
import javax.swing.*;
public class EmployeFrame extends JFrame{
   static boolean logedIn = false;
   EmployeFrame(){
      this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      this.setSize(new Dimension (600,400));
      this.setResizable(false);
      this.add(new EmployePanel());
      this.setLocationRelativeTo(null);
      this.setVisible(true);
   }
}
