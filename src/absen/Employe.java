package absen;

import java.util.Date;
import java.util.ArrayList;
public class Employe extends Person {
   
   private ArrayList <Presence> presence = new ArrayList<Presence>();
   
   Employe(String name, String username, String password) {
      super(name, username, password);
   }

   public void setPresence(boolean present) {
      if (presence.size() == 0){
         presence.add(new Presence(present));
      }
      else{
         if (presence.get(presence.size()-1).getDate().compareTo(new Date()) != 0 ){
            presence.add(new Presence(present));
         }
      }
   }  
}