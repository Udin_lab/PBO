package absen;

import java.awt.*;
import java.awt.event.*;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.*;
import javax.swing.border.LineBorder;

public class AdminPanel extends JPanel implements ActionListener{

   Color lightOrange = new Color(200, 84, 23  );
   Color darkOrange = new Color(150, 63, 17  );
   Color darkBg= new Color(34,31,46  );
   Color lightBg = new Color(46,43,63  );
   Color lightPurple = new Color(60,31,109  );
   Color darkPurple = new Color(83,31,162  );
   // Color 

   JTextField username;
   JPasswordField password;
   JButton login;
   JButton logout;
   boolean logedIn = false;

   JRadioButton buatAbsen;
   JRadioButton rekapAbsen;
   JRadioButton addEmploye;
   JRadioButton delEmploye;

   JPanel adminMainPanel;

   Admin curretntAdmin;

   Font font = Main.MyFont();

   Date today = new Date();
   SimpleDateFormat dateFor = new SimpleDateFormat("dd/MM/yyyy");
   JButton bukaAbsenHariIni;
   JButton tutupAbsenHariIni;

   AdminPanel(){
      this.setLayout(null);
      username = new JTextField();
      password = new JPasswordField();
      login = new JButton("LOGIN");
      login.addActionListener(this);
   }
   
   public void DrawUserInput(){

      password.setBorder(new LineBorder(lightOrange));
      username.setBorder(new LineBorder(lightOrange));
      login.setBorder(new LineBorder(lightOrange));

      username.setBackground(darkBg);
      password.setBackground(darkBg);
      login.setBackground(darkOrange);

      username.setForeground(Color.LIGHT_GRAY);
      password.setForeground(Color.LIGHT_GRAY);
      login.setForeground(Color.lightGray);

      login.setFocusable(false);
      
      username.setCaretColor(Color.lightGray);
      password.setCaretColor(Color.lightGray);

      //labels
      JLabel head = new JLabel("LOGIN SEBAGAI ADMIN");
      JLabel userLabel = new JLabel("Username");
      JLabel passLabel = new JLabel("Password");
      
      head.setForeground(Color.LIGHT_GRAY);
      userLabel.setForeground(Color.lightGray);
      passLabel.setForeground(Color.lightGray);
      
      head.setHorizontalAlignment(JLabel.CENTER);
      
      head.setFont(font.deriveFont(Font.BOLD, 20));
      username.setFont(font.deriveFont(Font.PLAIN, 14));
      password.setFont(font.deriveFont(Font.PLAIN, 14));
      userLabel.setFont(font.deriveFont(Font.PLAIN, 14));
      passLabel.setFont(font.deriveFont(Font.PLAIN, 14));
      login.setFont(font.deriveFont(Font.PLAIN, 16));
      
      username.setBounds(240,145,200,35);
      password.setBounds(240,190,200,35);
      login.setBounds(190,250,220,40);

      head.setBounds( 150 , 70, 300, 55);
      userLabel.setBounds(160,145,80,35);
      passLabel.setBounds(160,190,80,35);

      this.add(head);
      this.add(userLabel);
      this.add(passLabel);
      this.add(username);
      this.add(password);
      this.add(login);
   }

   void renderAbsen(){
      revalidate();
      repaint();

      adminMainPanel.setBackground(lightBg);
      adminMainPanel.setBounds( 120,70,470,290 );
      adminMainPanel.setLayout(null);
      
      JLabel absenTitle = new JLabel("Absen hari ini : " + dateFor.format(today));
      absenTitle.setBounds(10,10,450,50);
      absenTitle.setForeground(Color.lightGray);
      absenTitle.setFont(font.deriveFont(Font.BOLD,16));
      absenTitle.setHorizontalAlignment(JLabel.CENTER);

      bukaAbsenHariIni = new JButton("Buka absen hari ini");
      bukaAbsenHariIni.setBounds(135,70, 200,40);
      bukaAbsenHariIni.setBackground(Color.darkGray);
      bukaAbsenHariIni.addActionListener(this);
      bukaAbsenHariIni.setFocusable(false);
      bukaAbsenHariIni.setBorder(new LineBorder(lightOrange) );
      bukaAbsenHariIni.setFont(font.deriveFont(Font.PLAIN,15));
      bukaAbsenHariIni.setForeground(Color.lightGray);

      tutupAbsenHariIni = new JButton("Tutup absen hari ini");
      tutupAbsenHariIni.setBounds(135,125, 200,40);
      tutupAbsenHariIni.setBackground(Color.darkGray);
      tutupAbsenHariIni.addActionListener(this);
      tutupAbsenHariIni.setFocusable(false);
      tutupAbsenHariIni.setBorder(new LineBorder(lightOrange) );
      tutupAbsenHariIni.setFont(font.deriveFont(Font.PLAIN,15));
      tutupAbsenHariIni.setForeground(Color.lightGray);

      adminMainPanel.add(tutupAbsenHariIni);
      adminMainPanel.add(absenTitle);
      adminMainPanel.add(bukaAbsenHariIni);

      this.add(adminMainPanel);
   }

   void renderRekap(){
      revalidate();
      repaint();

   }

   void renderAddEmploye(){
      revalidate();
      repaint();
   }

   @Override
   public void paintComponent(Graphics g){
      if (!logedIn){
         DrawUserInput();
         username.setText("");
         password.setText("");
         g.setColor(darkBg);
         g.fillRect(0, 0, 600, 400);

         g.setColor(lightBg);
         //head
         g.fillRect( 150 , 70, 300, 55);
         //main
         g.fillRect( 150 , 135, 300, 100);
         //submit
         g.fillRect(150, 245, 300, 50);
   
         //line
         g.setColor(lightOrange);
         //head
         g.drawRect( 150 , 70, 300, 55);
         //main
         g.drawRect( 150 , 135, 300, 100);
         //submit
         g.drawRect(150, 245, 300, 50);

      }

      else if (logedIn){
         adminMainPanel = new JPanel();
         g.setColor(darkBg);
         g.fillRect(0, 0, 600, 400);

         //header
         g.setColor(lightBg);
         g.fillRect(10, 10, 100, 100);
         g.fillRect(120, 10, 350, 50); //470 max
         g.fillRect(10, 120, 100, 240 );
         
         g.setColor(darkOrange);
         g.fillOval(30, 30, 60, 60);
         g.setColor(lightOrange);
         g.drawOval(20, 20, 80, 80);

         logout = new JButton("Logout");
         logout.setBackground(darkOrange);
         logout.setForeground(Color.LIGHT_GRAY);
         logout.setFont(font.deriveFont(Font.BOLD, 15));
         logout.setBorder(new LineBorder(lightOrange));
         logout.setBounds(480,10,110,50 );
         logout.addActionListener(this);
         logout.setFocusable(false);

         ButtonGroup grup = new ButtonGroup();
         buatAbsen = new JRadioButton("Atur Absen");
         rekapAbsen = new JRadioButton("Rekapitulasi");
         addEmploye = new JRadioButton("tambah \nPegawai ");
         delEmploye = new JRadioButton("pecat \nPegawai ");
         grup.add(buatAbsen);
         grup.add(rekapAbsen);
         grup.add(addEmploye);
         grup.add(delEmploye);

         JLabel adminTitle = new JLabel("Admin : " + curretntAdmin.getName() );
         adminTitle.setForeground(Color.lightGray);
         adminTitle.setFont(font.deriveFont(Font.BOLD, 20));
         adminTitle.setBounds(130, 10, 350, 50);

         buatAbsen.setBorder(new LineBorder(darkOrange,1));
         buatAbsen.setBackground(lightBg);
         buatAbsen.setForeground(Color.LIGHT_GRAY);
         buatAbsen.setFocusable(false);
         buatAbsen.setFont(font.deriveFont(Font.PLAIN, 10));
         buatAbsen.setBounds(10, 150, 100, 40);
         buatAbsen.addActionListener(this);
         buatAbsen.setSelected(true);

         rekapAbsen.setBorder(new LineBorder(darkOrange,1));
         rekapAbsen.setBackground(lightBg);
         rekapAbsen.setForeground(Color.LIGHT_GRAY);
         rekapAbsen.setFocusable(false);
         rekapAbsen.setFont(font.deriveFont(Font.PLAIN, 10));
         rekapAbsen.setBounds(10, 200, 100, 40);
         rekapAbsen.addActionListener(this);

         addEmploye.setBorder(new LineBorder(darkOrange,1));
         addEmploye.setBackground(lightBg);
         addEmploye.setForeground(Color.LIGHT_GRAY);
         addEmploye.setFocusable(false);
         addEmploye.setFont(font.deriveFont(Font.PLAIN, 10));
         addEmploye.setBounds(10, 250, 100, 40);
         addEmploye.addActionListener(this);

         delEmploye.setBorder(new LineBorder(darkOrange,1));
         delEmploye.setBackground(lightBg);
         delEmploye.setForeground(Color.LIGHT_GRAY);
         delEmploye.setFocusable(false);
         delEmploye.setFont(font.deriveFont(Font.PLAIN, 10));
         delEmploye.setBounds(10, 300, 100, 40);
         delEmploye.addActionListener(this);

         if (buatAbsen.isSelected()){
            renderAbsen();
         }
         else if (rekapAbsen.isSelected()){
            renderRekap();
         }
         else if (addEmploye.isSelected()){
            renderAddEmploye();
         }
         else if (delEmploye.isSelected()){
            
         }

         this.add(logout);
         this.add(adminTitle);
         this.add(buatAbsen);
         this.add(rekapAbsen);
         this.add(addEmploye);
         this.add(delEmploye);

      }
   }

   @Override
   public void actionPerformed(ActionEvent e) {

      if (e.getSource() == login){
         String passInput = new String(password.getPassword());
         String userInput = username.getText();
         int index = 0;
         boolean found = false;
         for (int i = 0; i<Main.admins.size(); i++){
            String userData = Main.admins.get(i).getUsername();
            if (userData.equals(userInput)){
               index = i;
               found = true;
               break;
            }
         }
         if (found){
            String passData = Main.admins.get(index).getPassword();
            if (passInput.equals(passData)){
               logedIn = true;
               curretntAdmin = Main.admins.get(index);
               removeAll();
               repaint();
            }
            else{
               JOptionPane.showMessageDialog(this, "Username dan password admin tidak cocok","", JOptionPane.ERROR_MESSAGE);
            }
         }
         else{
            JOptionPane.showMessageDialog(this, "Admin tidak ditemukan, masukan akun dengan benar", "Admin tidak ditemukan", JOptionPane.ERROR_MESSAGE);
         }
      }

      else if (e.getSource() == logout){
         if (JOptionPane.showConfirmDialog(this, "Anda yakin akan logout?", "", JOptionPane.YES_NO_OPTION) == 0) {
            this.removeAll();
            logedIn = false;
            this.revalidate();
            this.repaint();
         }
      }

      else if (e.getSource() == buatAbsen ||
         e.getSource() == rekapAbsen ||
         e.getSource() == addEmploye ||
         e.getSource() == delEmploye
      ){
         adminMainPanel.removeAll();
      }

      else if (e.getSource() == bukaAbsenHariIni){
         if ( Main.dates.size() == 0 ){
            Main.isAbsenOpen = true;
            Main.dates.add(today);
            for (int i = 0; i<Main.employes.size(); i++ ){
               Main.employes.get(i).setPresence(false);
            }
         }
         else {
            if (Main.dates.get(Main.dates.size()-1).compareTo(today) == 0){
               JOptionPane.showMessageDialog(this, "Absen sudah dimulai beberapa waktu lalu", "", JOptionPane.ERROR_MESSAGE );
            }
            else{
               Main.isAbsenOpen = true;
               Main.dates.add(today);
               for (int i = 0; i<Main.employes.size(); i++ ){
                  Main.employes.get(i).setPresence(false);
               }
            }
         }
      }

      else if (e.getSource() == tutupAbsenHariIni){
         Main.isAbsenOpen = false;
      }
   }
}
