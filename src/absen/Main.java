package absen;

import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Date;

public class Main {
   static ArrayList<Employe> employes = new ArrayList<Employe>();
   static ArrayList<Admin> admins = new ArrayList<Admin>();
   static ArrayList<Date> dates = new ArrayList<Date>();
   static boolean isAbsenOpen = false;

   public static Font MyFont(){
      Font font  = null;
      try{
         font = Font.createFont(Font.TRUETYPE_FONT, new File("Poppins-Regular.ttf"));
         GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
         ge.registerFont(font);
         return font;
      } catch(IOException|FontFormatException e){
         System.out.println("failed imp");
      }
      return font;
   }
   public static void main(String[] args) {
      admins.add( new Admin("Udin", "udin", "123456") );
      admins.add( new Admin("admin", "admin", "admin") );

      employes.add(new Employe("Haidar", "haidar", "123456"));
      employes.add(new Employe("Didit", "didit", "123456"));
      employes.add(new Employe("Wahyu", "wahyu", "123456"));
      employes.add(new Employe("Sarah", "sarah", "123456"));

      new AdminFrame();
      // new EmployeFrame();
      // System.out.println(mimi n.getClass().getName());
   }

   
}
